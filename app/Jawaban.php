<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    //
    public function pertanyaan() {
        return $this->belongsTo('App\Pertanyaan', 'pertanyaan_id', 'id');
    }

    public function user() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function voteup() {
        return $this->hasMany('App\Vote', 'document_id', 'id')->where(['type' => 'jawaban','vote' => 'up']);
    }

    public function votedown() {
        return $this->hasMany('App\Vote', 'document_id', 'id')->where(['type' => 'jawabane','vote' => 'down']);
    }

}
