<?php

namespace App\Http\Controllers;

use App\Jawaban;
use App\User;
use App\Vote;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class VoteController extends Controller
{
    //

    function marked(Request $request, $id){
        $model = Jawaban::where([
            'id' => $id,
            'user_id' => $request->user_id,
        ])->first();
        if(isset($model) && $model->marked){
            return \Response::json(array(
                'code'      =>  401,
                'message'   =>  'error'
            ), 401);
        }else{
            $model->marked = 1;
            $model->update();
        }

        return \Response::json(array(
            'code'      =>  201,
            'message'   =>  'ok'
        ), 201);
    }
    function voting(Request $request, $id){
        $data = Vote::where([
            'document_id' => $id,
            'user_id' =>  Auth::user()->id,
            'vote' => $request->vote
        ])->first();
        
        if($data){
            return \Response::json(array(
                'code'      =>  401,
                'message'   =>  'error'
            ), 401);
        }
        
        $model = new Vote();
        $model->document_id = $id;
        $model->type = $request->type;
        $model->user_id = Auth::user()->id;
        $model->vote = $request->vote;

        $model->save();

        if($request->vote == 'up'){
            $user = User::where(['id' => $request->user_id])->first();
            $user->rating = $user->rating + 10;
            $user->update();
        }else{
            $user = User::where(['id' => Auth::user()->id])->first();
            $user->rating = $user->rating - 1;
            $user->update();
        }
        
        return \Response::json(array(
            'code'      =>  201,
            'message'   =>  'ok'
        ), 201);
    }
}
