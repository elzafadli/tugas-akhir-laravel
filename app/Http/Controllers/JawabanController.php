<?php

namespace App\Http\Controllers;

use App\Jawaban;
use App\Pertanyaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JawabanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $data = Pertanyaan::where(['id' => $id])->first();

        return view('user.jawaban.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $data = Pertanyaan::where(['id' => $id])->first();

        return view('user.jawaban.form', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Jawaban();
        $model->pertanyaan_id = $request->pertanyaan_id;
        $model->isi = $request->isi;
        $model->user_id = Auth::user()->id;

        $model->save();

        return redirect('jawaban/view/'.$model->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Jawaban::where(['id' => $id])->first();

        return view('user.jawaban.view', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $data = Jawaban::where(['id' => $id])->first();
        
        return view('user.jawaban.form', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Jawaban::where(['id' => $request->id])->first();

        $model->pertanyaan_id = $request->pertanyaan_id;
        $model->isi = $request->isi;
        $model->user_id = Auth::user()->id;

        $model->update();
        
        return redirect('jawaban/'.$model->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
