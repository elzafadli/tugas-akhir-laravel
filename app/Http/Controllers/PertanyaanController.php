<?php

namespace App\Http\Controllers;

use App\Pertanyaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PertanyaanController extends Controller
{
    public function index()
    {
        $data = Pertanyaan::get();
        return view('user.pertanyaan.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.pertanyaan.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Pertanyaan();
        $model->judul = $request->judul;
        $model->isi = $request->isi;
        $model->tag = implode(',',$request->tag);
        $model->user_id = Auth::user()->id;

        $model->save();

        return redirect('');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Pertanyaan::where(['id' => $id])->first(); 
        $data->tag = explode(',', $data->tag);

        return view('user.pertanyaan.view', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Pertanyaan::where(['id' => $id])->first();
        $data->tag = isset($data->tag) ? explode(',', $data->tag) : '';

        return view('user.pertanyaan.form', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Pertanyaan::where(['id' => $request->id])->first();

        $model->judul = $request->judul;
        $model->isi = $request->isi;
        $model->tag = isset($request->tag) ? implode(',',$request->tag) : '';

        $model->update();
        
        return redirect('pertanyaan/'.$request->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $model = Pertanyaan::where(['id' => $id])->first();

        $model->delete();
        
        return redirect('pertanyaan');
    }
}
