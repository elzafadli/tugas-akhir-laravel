<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    public function jawaban() {
        return $this->hasMany('App\Jawaban', 'pertanyaan_id', 'id');
    }

    public function user() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function voteup() {
        return $this->hasMany('App\Vote', 'document_id', 'id')->where(['type' => 'pertanyaan','vote' => 'up']);
    }

    public function votedown() {
        return $this->hasMany('App\Vote', 'document_id', 'id')->where(['type' => 'pertanyaan','vote' => 'down']);
    }
}
