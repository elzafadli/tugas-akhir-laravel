<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'PertanyaanController@index');
Route::get('/home', 'PertanyaanController@index');
Route::group(['middleware' => 'auth'], function () {
    Route::get('/pertanyaan/create', 'PertanyaanController@create');
    Route::post('/pertanyaan/create', 'PertanyaanController@store');
    Route::post('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update');  
    Route::get('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@show');
    Route::get('/pertanyaan/edit/{pertanyaan_id}', 'PertanyaanController@edit'); 
    Route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy')->name('pertanyaan.destroy');

    Route::get('/jawaban/create/{pertanyaan_id}', 'JawabanController@create');
    Route::get('/jawaban/{pertanyaan_id}', 'JawabanController@index');
    Route::get('/jawaban/view/{pertanyaan_id}', 'JawabanController@show');
    Route::get('/jawaban/edit/{pertanyaan_id}', 'JawabanController@edit');
    Route::post('/jawaban/{pertanyaan_id}', 'JawabanController@store');

    Route::post('/vote/{pertanyaan_id}', 'VoteController@voting');
    Route::post('/vote/marked/{pertanyaan_id}', 'VoteController@marked');
});

