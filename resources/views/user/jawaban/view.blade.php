@extends('layouts.app')

@section('content')
<div class="container">
    <a style="margin: 10px 0" href="{{url('jawaban/'.$data->pertanyaan->id)}}" class="btn btn-primary">View Answer</a>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Pertanyaan</label>
                <input type="text" disabled class="form-control" required value="{{$data->pertanyaan->judul}} ?">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Jawaban</label>
                <input type="text" disabled name="isi" class="form-control" required value="{{$data->isi}}" ?>
            </div>
        </div>
    </div>

    <div class="box-footer">
        <a href="{{ url('jawaban/edit/'.$data->id.'/') }}" type="submit" class="btn btn-primary">Edit</a>
    </div>

</div>
@endsection
