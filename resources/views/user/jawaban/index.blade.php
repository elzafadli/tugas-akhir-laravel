@extends('layouts.app')

@section('content')
<div class="container">
    <div>
        <h1>Question : {{$data->judul}} ? </h1>
        
        @if(isset(Auth::user()->id) && Auth::user()->id != $data->user_id )
            <a style="margin-top: 10px" href="{{url('jawaban/create/'.$data->id)}}" class="btn btn-primary">Answer Question</a>
        @endif
    </div>
    <div class="row">
        <div class="card"></div>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Answer</th>
                    <th scope="col">Voting</th>
                    <th scope="col">Marked</th>
                    <th scope="col">Created By</th>
                    <th scope="col">Time Create</th>
                    <th scope="col">Time Update</th>
                    <th scope="col" style="width: 20%">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data->jawaban as $r)
                    <tr>
                        <th scope="row">{{ $r->isi }}</th>
                        <th scope="row">{{count($r->voteup) - count($r->votedown) }}</th>
                        <th scope="row">{{ $r->marked ? "Yes" : 'No' }}</th>
                        <th scope="row">{{ $r->user->name }} - Rating : {{ $r->user->rating }} </th>
                        <th scope="row">{{$r->created_at}}</th>
                        <th scope="row">{{$r->updated_at}}</th>
                        <td>
                            @if(isset(Auth::user()->id) && Auth::user()->id != $r->user_id )
                                <button style="margin-top: 10px" type="button" class="btn btn-success" onclick="vote({{$r->id}},{{$r->user_id}})">
                                    Vote <span class="badge badge-light">{{count($r->voteup)}}</span>
                                </button>
                            @endif
                            
                            @if(isset(Auth::user()->id) && Auth::user()->id != $r->user_id && Auth::user()->rating > 10 )
                                <button style="margin-top: 10px" type="button" class="btn btn-danger" onclick="downvote({{$r->id}},{{$r->user_id}})">
                                    Down Vote <span class="badge badge-light">{{count($r->votedown)}}</span>
                                </button>
                            @endif
                            
                            @if(isset(Auth::user()->id) && Auth::user()->id != $r->user_id)
                                <button style="margin-top: 10px" type="button" class="btn btn-primary" onclick="marked({{$r->id}},{{$r->user_id}})">
                                    Marked
                                </button>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<script>
    function vote(id, user_id){
        $.ajax({
            type : 'post',
            url : '{{ url('vote') }}' + '/' + id,
            data: {
                "_token": "{{ csrf_token() }}",
                "type" : 'jawaban',
                "user_id" : user_id,
                'vote' : 'up'
            },
            success: function (data) {
                Swal.fire({
                    title: 'Berhasil!',
                    text: 'Voting berhasil!',
                    icon: 'success',
                    confirmButtonText: 'OK'
                });
                location.reload();
            },
            error: function (error) {
                Swal.fire({
                    title: 'Gagal!',
                    text: 'Already Vote!',
                    icon: 'error',
                    confirmButtonText: 'OK'
                });
            }
        });
    }

    function vote(id, user_id){
        $.ajax({
            type : 'post',
            url : '{{ url('vote') }}' + '/' + id,
            data: {
                "_token": "{{ csrf_token() }}",
                "type" : 'jawaban',
                "user_id" : user_id,
                'vote' : 'up'
            },
            success: function (data) {
                Swal.fire({
                    title: 'Berhasil!',
                    text: 'Voting berhasil!',
                    icon: 'success',
                    confirmButtonText: 'OK'
                });
                location.reload();
            },
            error: function (error) {
                Swal.fire({
                    title: 'Gagal!',
                    text: 'Already Vote!',
                    icon: 'error',
                    confirmButtonText: 'OK'
                });
            }
        });
    }

    function marked(id, user_id){
        $.ajax({
            type : 'post',
            url : '{{ url('vote') }}' + '/marked/' + id,
            data: {
                "_token": "{{ csrf_token() }}",
                "user_id" : user_id,
            },
            success: function (data) {
                Swal.fire({
                    title: 'Berhasil!',
                    text: 'Marked Success!',
                    icon: 'success',
                    confirmButtonText: 'OK'
                });
                location.reload();
            },
            error: function (error) {
                Swal.fire({
                    title: 'Gagal!',
                    text: 'Already Marked!',
                    icon: 'error',
                    confirmButtonText: 'OK'
                });
            }
        });
    }
</script>
@endsection
