@extends('layouts.app')

@section('content')
<div class="container">

    <form method="post" action="{{ url('jawaban/create') }}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="pertanyaan_id" value={{$data->id}}>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Pertanyaan</label>
                    <input type="text" value="{{isset($data->judul) ? $data->judul  : '' }} ?" disabled class="form-control" required >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Jawaban</label>
                    <input type="text" name="isi" class="form-control" required value="{{isset($data->pertanyaan) ? $data->pertanyaan->isi  : '' }}">
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>

</div>

<script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
<script>
    tinymce.init({
        selector:'textarea',
        width: 900,
        height: 300
    });
</script>

@endsection


