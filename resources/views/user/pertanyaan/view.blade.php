@extends('../layouts.app')

@section('content')
<div class="container">
    <form method="post" action="{{ isset($data->id) ?  url('pertanyaan/'.$data->id) : url('pertanyaan/create') }}">
        @csrf
        @isset($data->id)
        <input type="hidden" name="id" value="{{$data->id}}">
        @endisset
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Judul</label>
                    <input type="text" name="judul" class="form-control" required placeholder="Enter Judul"
                        value="{{isset($data->judul) ? $data->judul : '' }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Isi</label>
                    <textarea type="text" name="isi" class="form-control">{!! $data->isi !!}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label for="formGroupExampleInput">Tag</label>
                <div class="form-group">
                    <div class="form-check form-check-inline" >
                        <input class="form-check-input" type="checkbox" disabled  {{ in_array('Politik', $data->tag) ? 'checked' : '' }} name="tag[]" value="Politik">
                        <label class="form-check-label">
                            Politik
                        </label>
                    </div>
                    <div class="form-check form-check-inline" >
                        <input class="form-check-input" type="checkbox" disabled {{ in_array('Komedi', $data->tag) ? 'checked' : '' }} name="tag[]" value="Komedi">
                        <label class="form-check-label">
                            Komedi
                        </label>
                    </div>
                    <div class="form-check form-check-inline" >
                        <input class="form-check-input" type="checkbox" disabled {{ in_array( 'Mebur', $data->tag) ? 'checked' : '' }} name="tag[]" value="Mebur">
                        <label class="form-check-label">
                            Mebur
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tanggal Dibuat</label>
                    <input type="text" class="form-control" disabled
                        value="{{isset($data->created_at) ? $data->created_at : ''}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Tanggal Diperbaharui</label>
                    <input type="text" class="form-control" disabled
                        value="{{isset($data->updated_at) ? $data->updated_at : ''}}">
                </div>
            </div>
        </div>

        <div class="box-footer">
            <a href="{{ url('pertanyaan/edit/'.$data->id.'/') }}" type="submit" class="btn btn-primary">Edit</a>
        </div>
</div>

@endsection
