@extends('layouts.app')

@section('content')
<div class="container">
    <div class="cheader">
        <h1>Question</h1>
        <a href="{{url('pertanyaan/create')}}" class="btn btn-primary">Ask Question</a>
    </div>
    <div class="row">
        <div class="card"></div>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Question</th>
                    <th scope="col">Tag</th>
                    <th scope="col">Voting</th>
                    <th scope="col">Created By</th>
                    <th scope="col">Time Create</th>
                    <th scope="col">Time Update</th>
                    <th scope="col" style="width: 20%">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $r)
                    <tr>
                        <th scope="row">{{ $r->judul }} ?</th>
                        <th scope="row">
                            @foreach (explode(',', $r->tag) as $t)
                                <span class="badge badge-success">{{$t}}</span>
                            @endforeach
                        </th>
                        <th scope="row">{{count($r->voteup) - count($r->votedown) }}</th>
                        <th scope="row">{{ $r->user->name }} - Rating : {{ $r->user->rating }} </th>
                        <th scope="row">{{$r->created_at}}</th>
                        <th scope="row">{{$r->updated_at}}</th>
                        <td>
                            @if(isset(Auth::user()->id) && Auth::user()->id != $r->user_id )
                                <button style="margin-top: 10px" type="button" class="btn btn-success" onclick="vote({{$r->id}},{{$r->user_id}})">
                                    Vote <span class="badge badge-light">{{count($r->voteup)}}</span>
                                </button>

                                @if(Auth::user()->rating > 10 )
                                    <button style="margin-top: 10px" type="button" class="btn btn-danger" onclick="downvote({{$r->id}},{{$r->user_id}})">
                                        Down Vote <span class="badge badge-light">{{count($r->votedown)}}</span>
                                    </button>
                                @endif
                            @else
                                <a style="margin-top: 10px" href="{{url('pertanyaan/'.$r->id)}}" type="button" class="btn btn-primary">
                                    View
                                </a>
                                <a style="margin-top: 10px" href="{{url('pertanyaan/edit/'.$r->id)}}" type="button" class="btn btn-success">
                                    Edit
                                </a>
                            @endif
                            
                                
                            <a style="margin-top: 10px" href="{{url('jawaban/'.$r->id)}}" type="button" class="btn btn-secondary">
                                Answer <span class="badge badge-light">{{count($r->jawaban)}}</span>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@push('scripts')
<script>
    function vote(id, user_id){
        $.ajax({
            type : 'post',
            url : '{{ url('vote') }}' + '/' + id,
            data: {
                "_token": "{{ csrf_token() }}",
                "type" : 'pertanyaan',
                "user_id" : user_id,
                'vote' : 'up'
            },
            success: function (data) {
                Swal.fire({
                    title: 'Berhasil!',
                    text: 'Voting berhasil!',
                    icon: 'success',
                    confirmButtonText: 'OK'
                });
                location.reload();
            },
            error: function (error) {
                Swal.fire({
                    title: 'Gagal!',
                    text: 'Already Vote!',
                    icon: 'error',
                    confirmButtonText: 'OK'
                });
            }
        });
    }

    function downvote(id, user_id){
        $.ajax({
            type : 'post',
            url : '{{ url('vote') }}' + '/' + id,
            data: {
                "_token": "{{ csrf_token() }}",
                "type" : 'pertanyaan',
                "user_id" : user_id,
                'vote' : 'down'
            },
            success: function (data) {
                Swal.fire({
                    title: 'Berhasil!',
                    text: 'Voting berhasil!',
                    icon: 'success',
                    confirmButtonText: 'OK'
                });
                location.reload();
            },
            error: function (error) {
                Swal.fire({
                    title: 'Gagal!',
                    text: 'Already Vote!',
                    icon: 'error',
                    confirmButtonText: 'OK'
                });
            }
        });
    }
</script>
@endpush

